#ifndef CONFIG_H
#define CONFIG_H

#include <sstream>

#ifndef NDEBUG
#define EXP_DEBUG
#endif

#ifdef EXP_DEBUG

#define EXP_ASSERT(X, MSG) do { \
    if (!X) { \
        std::stringstream msg; \
        msg << "[DEBUG] In " << __FILE__ << " at line: " << __LINE__ << " in function: " << __func__ << "\n" \
            << MSG << "\n" \
            << "  assertion failed: " << #X << "\n" \
            << std::endl; \
        std::cout << msg.str(); \
    } \
} while (false)

#define EXP_MESSAGE(MSG) do { \
    std::stringstream msg; \
    msg << "[DEBUG] In " << __FILE__ << " at line: " << __LINE__ << " in function: " << __func__ << "\n" \
        << MSG << "\n" \
        << std::endl; \
    std::cout << msg.str(); \
} while (false)

#else  // ifdef EXP_DEBUG

#define EXP_ASSERT(X, MSG)
#define EXP_MESSAGE(MSG)

#endif  // ifdef EXP_DEBUG

#endif  // CONFIG_H
