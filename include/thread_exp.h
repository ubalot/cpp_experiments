#ifndef THREAD_EXP_H
#define THREAD_EXP_H

#include <chrono>
#include <functional>
#include <mutex>
#include <thread>

#include "config.h"

namespace thread_exp {

    template <typename T>
    void print(const T msg);

    class ThreadExp {
    private:
        std::string m_name;
#ifdef EXP_DEBUG
        std::chrono::time_point<std::chrono::system_clock> m_start_time;
#endif
        std::thread m_thread;

    public:
        explicit ThreadExp(std::string name, std::function<void()> f);
        // template <typename Function>
        // explicit ThreadExp(std::string name, Function&& f);
        // template <typename Function, typename... Args>
        // explicit ThreadExp(std::string name, Function&& f, Args&&... args);
        ~ThreadExp();
        const std::string get_name() const;
        uint64_t get_id() const;

        static uint64_t from_id_to_ull(const std::thread::id id);
    };

};

#endif  //THREAD_EXP_H
