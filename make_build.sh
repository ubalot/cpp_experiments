#!/usr/bin/env bash

build_dir=build
debug_dir="$build_dir"/debug
release_dir="$build_dir"/release

debug=0
case "$1" in
    clean) rm -rf "$build_dir" ; exit 0 ;;
    debug) debug=1 ;;
esac

###################################
# CMAKE compile                   #
###################################
cmake_flags=()
if [[ $debug == 1 ]]
then
    mkdir -p "$debug_dir"
    cd "$debug_dir" || exit 1
    cmake_flags+=("-DCMAKE_BUILD_TYPE=Debug")
    target="debug"
else
    mkdir -p "$release_dir"
    cd "$release_dir" || exit 1
    cmake_flags+=("-DCMAKE_BUILD_TYPE=Release")
    target="release"
fi
cmake "${cmake_flags[@]}" ../..

cd ..
cmake --build "$target"
ret=$?

###################################
# Log to user                     #
###################################
echo
if [[ $ret == 0 ]]
then
	echo " [*] $(basename "$0"): success"
else
	echo " [X] $(basename "$0"): failure"
fi
echo

exit $ret
