# CPP experiments

## Clean
```bash
./make_build.sh clean
```

## Debug

#### Build
```bash
./make_build.sh debug
```
#### Run
```bash
./build/debug/experiments
```

## Release

#### Build
```bash
./make_build.sh release
```
#### Run
```bash
./build/release/experiments
```
