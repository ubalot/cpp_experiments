#include <chrono>
#include <ctime>
#include <iomanip>
#include <iostream>
#include <sstream>

#include "thread_exp.h"

using namespace thread_exp;

template <typename T>
void thread_exp::print(const T msg)
{
    static std::mutex cout_mtx;
    const std::lock_guard<std::mutex> lock(cout_mtx);
    std::cout << msg << std::endl;
}

uint64_t ThreadExp::from_id_to_ull(const std::thread::id id)
{
    std::stringstream ss;
    ss << id;
    return std::stoull(ss.str());
}

ThreadExp::ThreadExp(std::string name, std::function<void()> f)
    : m_name{name}
#ifdef EXP_DEBUG
    , m_start_time{std::chrono::system_clock::now()}
#endif
    , m_thread{std::thread(f)}
{
}

// template <typename Function>
// ThreadExp::ThreadExp(std::string name, Function&& f)
//     : m_name{name}
// #ifdef EXP_DEBUG
//     , m_start_time{std::chrono::system_clock::now()}
// #endif
//     , std::thread(std::forward<Function>(f))
// // std::forward<_Callable>(__f), std::forward<_Args>(__args)...
// {
// }

// template <typename Function, typename... Args>
// ThreadExp::ThreadExp(std::string name, Function&& f, Args&&... args)
//     : m_name{name}
// #ifdef EXP_DEBUG
//     , m_start_time{std::chrono::system_clock::now()}
// #endif
//     , std::thread(f, args...)
// {
// }

ThreadExp::~ThreadExp()
{
#ifdef EXP_DEBUG
    auto thread_id {m_thread.get_id()};
#endif

    if (m_thread.joinable())
        m_thread.join();

#ifdef EXP_DEBUG
    const auto now {std::chrono::system_clock::now()};
    std::chrono::duration<double> elapsed_seconds {now - m_start_time};
    std::time_t start_time {std::chrono::system_clock::to_time_t(m_start_time)};
    std::time_t end_time {std::chrono::system_clock::to_time_t(now)};
    const auto datetime_format {"%Y/%m/%d %H:%M:%S"};
    std::stringstream out;
    out << "Thread: " << "name " << m_name << " , " << "id: " << thread_id << "\n"
        << std::left
        << std::setw(14) << "Living time: " << elapsed_seconds.count() << " seconds" << "\n"
        << std::setw(14) << "Started at: " << std::put_time(localtime(&start_time), datetime_format) << "\n"
        << std::setw(14) << "Ended at: " << std::put_time(localtime(&end_time), datetime_format) << std::endl;
    print(out.str());
#endif
}

const std::string ThreadExp::get_name() const
{
    return m_name;
}

uint64_t ThreadExp::get_id() const
{
    return from_id_to_ull(std::this_thread::get_id());
}
