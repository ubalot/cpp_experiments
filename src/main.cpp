#include <iostream>
#include <memory>
#include <mutex>
#include <sstream>
#include <vector>

#include "stacktrace.h"
#include "thread_exp.h"

void print_defines()
{
    constexpr int line_length {20};

    std::stringstream ss;

#define LINE_WITH_DOTS(X) \
    (ss << X << " " << std::string(line_length - std::string(X).size() - 2, '.' ) << " ")

    LINE_WITH_DOTS("EXP_DEBUG")
#ifdef EXP_DEBUG
    << "defined"
#else
    << "not defined"
#endif
    << "\n";

    LINE_WITH_DOTS("EXP_ASSERT")
#ifdef EXP_ASSERT
    << "defined"
#else
    << "not defined"
#endif
    << "\n";

    LINE_WITH_DOTS("EXP_MESSAGE")
#ifdef EXP_MESSAGE
    << "defined"
#else
    << "not defined"
#endif
    << "\n";

    std::cout << ss.str() << std::endl;

#undef LINE_WITH_DOTS
}

void f(){
    std::this_thread::sleep_for( std::chrono::milliseconds(2000) );
}

void app()
{
    print_defines();

    const uint64_t main_thread_id { thread_exp::ThreadExp::from_id_to_ull( std::this_thread::get_id() ) };

    std::stringstream ss;
    ss << "main :: thread id: " << main_thread_id << std::endl;
    thread_exp::print(ss.str());

    // std::vector< std::unique_ptr<thread_exp::ThreadExp> > v;
    for (int i = 0; i < 10; i++)
    {
        std::string thread_name { std::string("my_thread_") + std::to_string(i) };

        // std::function<void()> func = [](){
        //     std::this_thread::sleep_for( std::chrono::milliseconds(2000) );
        // };

        thread_exp::ThreadExp(thread_name, f);
        // v.push_back( std::make_unique<thread_exp::ThreadExp>(thread_name, func) );
    }
    // v.clear();  // wait for all threads to join
    std::this_thread::sleep_for( std::chrono::seconds(10) );
}

int main(int /* argc */, char **/* argv */)
{
    try
    {
        app();
    }
    catch(...)
    {
        print_stacktrace();
    }

    return 0;
}

